module Main (main) where

import           Grassify
import           Grassify.Nums

main :: IO ()
main = plantGrass grassMain

grassMain :: Grassify Ref
grassMain =
  do outRec <- function2 $ \f n -> do {
       _ <- GOut $. (n $. GSucc $. GW);
       f $. f
     }
     out <- outRec $. outRec
     let hello = fmap natOfChar "Hello, world!\n"
     enc <- createNatEncodingFor hello
     puts enc out hello

puts :: NatEncoding -> Ref -> [Int] -> Grassify Ref
puts _   _    [] = function1 $ \x -> return x
puts enc outR (c:cs) =
  do n <- require (gNatBy enc c)
     r' <- outR $. n
     puts enc r' cs
