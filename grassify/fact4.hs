module Main(main) where

import           Grass.Eval
import           Grassify
import           Grassify.Nums
import           Grassify.Prelude

testProgram :: Grassify Ref -> IO (GObj IO)
testProgram = eval . getGrassCode

main :: IO ()
main = plantGrass grassMain

grassMain :: Grassify Ref
grassMain =
  do fact <- factCode
     two <- require (gNat 2)
     fact $. (two $. two) $. GOut $. GW

factCode :: Grassify Ref
factCode =
  do one  <- require (gNat 1)
     k    <- require gConst
     mult <- require gNatMult
     pred' <- require gNatPred
     -- factLoop f n = n * f (n - 1)
     factLoop <- function2 $
       \f n -> mult $. n $. (f $. (pred' $. n))
     iszero <- require gIsZero
     -- > fact' f n = if (n == 0) then
     -- >                 const (const 1) f n  (= 1)
     -- >             else
     -- >                 factLoop f n         (= n * f (n - 1))
     fact' <- function2 $
       \f n -> do factStop <- (k $. (k $. one))
                  cont <- iszero $. n $. factStop $. factLoop
                  cont $. f $. n
     gfix <- require gFix
     factRef <- gfix $. fact'
     return factRef

