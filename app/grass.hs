module Main(main) where

import Grass.Base
import Grass.Eval

import System.IO
import System.Environment

printUsage :: IO ()
printUsage = putStrLn "need to specify grass script file."

runScript :: FilePath -> IO ()
runScript path = do {
  hFile <- openFile path ReadMode;
  hSetEncoding hFile utf8;
  script <- hGetContents hFile;
  _ <- eval $ parseGrass script;
  return ()
}

main :: IO ()
main = getArgs >>= \args ->
       case args of
         []     -> printUsage
         path:_ -> runScript path
