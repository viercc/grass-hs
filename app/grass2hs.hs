module Main(main) where

import Grass.Base
import Grass.Grass2Hs

import System.IO
import System.Environment

printUsage :: IO ()
printUsage = putStrLn "Usage: grass2hs FILE"

printTranslated :: FilePath -> IO ()
printTranslated srcPath = 
  withFile srcPath ReadMode $ \hScriptFile ->
    do hSetEncoding hScriptFile utf8
       script <- hGetContents hScriptFile
       putStr $ translate (parseGrass script)
       return ()

data Option = PrintUsage |
              Normal FilePath -- ^ source

parseArgs :: [String] -> Option
parseArgs [spath] = Normal spath
parseArgs _ = PrintUsage

main :: IO ()
main = getArgs >>= \args ->
         case parseArgs args of
           PrintUsage -> printUsage
           Normal spath -> printTranslated spath
