module Template(
  Template(), readTemplate, fillTemplate
) where

import Data.Maybe (fromMaybe)
import qualified Data.Text.Prettyprint.Doc as PP
import           Text.ParserCombinators.ReadP

newtype Template = Template {
    fillTemplate :: (String -> Maybe (PP.Doc ())) -> PP.Doc ()
  }

readTemplate :: String -> Template
readTemplate str = Template (makeTemplate (lines str))

makeTemplate :: [String] -> (String -> Maybe (PP.Doc ())) -> PP.Doc ()
makeTemplate template content =
    PP.vcat $ map subst template
  where
    subst line =
      fromMaybe (PP.pretty line) $ readMacro line >>= content'
    
    content' (prefix, name) =
      fmap (\body -> PP.pretty prefix PP.<> body) (content name)

readMacro :: String -> Maybe (String, String)
readMacro = getOneMatch . readP_to_S parseLine where
  parseLine =
    do prefix <- many get
       name <- between (string "#{") (string "}#") (many get)
       eof
       return (prefix, name)

  getOneMatch []        = Nothing
  getOneMatch ((a,_):_) = Just a
