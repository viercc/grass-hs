{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Grassify.Golf(
  Expr(..), OpType(..),
  findCompact
) where

import           Control.Arrow                ((&&&))
import           Control.Monad

import           Data.Function                (on)
import           Data.List                    (sortBy)
import           Data.Maybe                   (fromMaybe)
import           Data.Ord

import           Data.IntMap.Strict           (IntMap)
import qualified Data.IntMap.Strict           as IMap
import           Data.IntSet                  (IntSet)
import qualified Data.IntSet                  as ISet

import qualified Data.MemoCombinators         as Memo

import           Math.NumberTheory.Logarithms (intLog2)
import           Math.NumberTheory.Powers     (exactRoot)

import           Optim

-- * Nat representations
data OpType = Add | Mul | Pow
            deriving (Show, Eq, Ord)

data Expr = Given Int | N Int | Op OpType Expr Expr
          deriving (Show, Eq, Ord)

decompose' :: Int -> [Expr]
decompose' n
  | n < 0               = negativeError n
  | (0 <= n) && (n < 3) = [Given n]
  | otherwise           =
      let byAdd = [Op Add (N j) (N (n-j)) | j <- [1..n `div` 2] ]
          byMul = [Op Mul (N j) (N k) | (j,k) <- factors n ]
          byPow = [Op Pow (N j) (N k) | (j,k) <- powInv n ]
      in byPow ++ byMul ++ byAdd

decompose :: Int -> [(Expr, IntSet)]
decompose = Memo.integral (simplify . decompose')

simplify :: [Expr] -> [(Expr, IntSet)]
simplify = elimBy (ISet.isSubsetOf `on` snd) .
           sortBy (comparing (approx . snd)) .
           fmap (id &&& exprToSet)
  where
    approx = sumISet
    elimBy isBetter =
      let go []     = []
          go (x:xs) = x : go (filter (\y -> not (x `isBetter` y)) xs)
      in go

exprToSet :: Expr -> IntSet
exprToSet (Given _)    = ISet.empty
exprToSet (N n)        = ISet.singleton n
exprToSet (Op _ e1 e2) = exprToSet e1 `ISet.union` exprToSet e2

negativeError :: Int -> any
negativeError n = error $ "Illegal negative argument: " ++ show n

exprWeight :: Expr -> Int
exprWeight (Op op _ _) =
  case op of
      Add -> 5
      Mul -> 3
      Pow -> 2
exprWeight _ = 0

-- * Minimization

data Target = Target {
    _from       :: !(IntMap Expr)
  , _fromWeight :: !Int
  , _requested  :: !IntSet
  }
  deriving (Show, Eq)

heuristic :: Target -> Int
heuristic (Target _ fromW to) = fromW + sumISet to

instance Optimizable Target where
  superior a b = (heuristic a <= heuristic b) &&
                 toA `ISet.isSubsetOf` toB
    where Target _ _ toA = a
          Target _ _ toB = b

  compareHeuristic = comparing heuristic

findCompactM :: Optim Target ()
findCompactM =
  do Target from fromW to <- askState
     case ISet.minView to of
       Nothing -> return ()
       Just (n, to') ->
         do advance (const (Target from fromW to'))
            if n `IMap.member` from
              then return ()
              else let ops = zip (repeat n) (decompose n)
                   in branch step ops >> return ()
            findCompactM
  where
    step (Target from fromW to) (n,(expr,exprSet)) =
      let newReqs = exprSet \\ IMap.keysSet from
          fromW' = fromW + exprWeight expr
      in Target (IMap.insert n expr from) fromW' (newReqs \/ to)

findCompact :: [Int] -> [Int] -> IntMap Expr
findCompact givenNats target =
  let givenNats' = [0..3] ++ givenNats
      startCalculated = IMap.fromList (fmap (id &&& Given) givenNats')
      startRequired = ISet.fromList target
      start = Target startCalculated 0 startRequired

      results = fmap snd $ runOptimization findCompactM start
      best = fromMaybe (error "No solution found!?") results
  in _from best

-- * Maths

-- | factor n = [(j,k) | j <- [2..], j*k == n, j <= k]
factors :: Int -> [(Int,Int)]
factors n =
  do j <- takeWhile (\j -> j*j <= n) [2..]
     let (k,r) = n `divMod` j
     guard (r == 0)
     return (j,k)

-- | powInv n = [(j,k) | j <- [2..], k <- [2..], j^k == n]
powInv :: Int -> [(Int, Int)]
powInv n =
  do k <- [2 .. intLog2 n]
     case exactRoot k n of
       Nothing -> []
       Just j  -> return (j, k)

-- * Utilities

sumISet :: IntSet -> Int
sumISet = ISet.foldl' (+) 0

(\/), (\\) :: IntSet -> IntSet -> IntSet
(\/) = ISet.union
(\\) = (ISet.\\)
