module Grassify.Nums(
  gNat,
  NatEncoding(),
  createNatEncodingFor,
  gNatBy,
  gIsZero,
  gNatSucc, gNatAdd, gNatMult,
  gNatPred,
  natOfChar, gNatOfChar
) where

import           Control.Monad.State

import           Data.Char           (ord)
import           Data.Word

import qualified Data.IntMap         as IMap
import qualified Data.Map.Strict     as Map

import           Grassify
import           Grassify.Golf
import           Grassify.Prelude

-- | Church Numerals
gNat :: Int -> Definition
gNat 0 = natDef 0 (require gFalse)
gNat 1 = natDef 1 (require gId)
gNat 2 = natDef 2 (function2 $ \f x -> f $. (f $. x))
gNat 3 = natDef 3 (function2 $ \f x -> f $. (f $. (f $. x)))
gNat n
  | n > 3 = natDef n $ do enc <- createNatEncodingFor [n]
                          require (gNatBy enc n)
  | otherwise = error $ "negative integer:" ++ show n

data NatEncoding = NatEncoding { _exprs :: IMap.IntMap Expr }
   deriving (Show)

createNatEncodingFor :: [Int] -> Grassify NatEncoding
createNatEncodingFor ns =
  do predefs <- getDefinedCNats
     return $ NatEncoding (findCompact (fmap fst predefs) ns)

gNatBy :: NatEncoding -> Int -> Definition
gNatBy enc = gNatBy'
  where
    gNatBy' n
      | n <= 3 = gNat n
      | otherwise = natDef n $
          case IMap.lookup n (_exprs enc) of
            Nothing   -> require (gNat n)
            Just expr -> go expr

    getRegistered n = require (natDef n $ error "Reference to undefined")

    go :: Expr -> Grassify Ref
    go (Op Add (N 1) e2) =
      do r2 <- go e2
         function2 $ \s z -> r2 $. s $. (s $. z)
    go (Op Add e1 (N 2)) =
      do r1 <- go e1
         function2 $ \s z -> r1 $. s $. (s $. z)
    go (Op op e1 e2) = makeOp op e1 e2
    go (Given n) = getRegistered n
    go (N n)     = require (gNatBy' n)

    makeOp Pow e1 e2 =
      do r1 <- go e1
         r2 <- go e2
         r2 $. r1
    makeOp Add e1        e2 | e1 > e2 = makeOp Add e2 e1
    makeOp Add (Given n) e2 | n <= 3  = makeSucc n e2
    makeOp Add (N n)     e2 | n <= 3  = makeSucc n e2
    makeOp Add e1        e2           = makeAdd e1 e2

    makeOp Mul e1        e2 | e1 > e2 = makeOp Mul e2 e1
    makeOp Mul (Given n) e2 | n <= 3  = makeIterAdd n e2
    makeOp Mul (N n)     e2 | n <= 3  = makeIterAdd n e2
    makeOp Mul e1        e2           = makeMul e1 e2

    iter n f x
      | n <= 0    = return x
      | otherwise = do y <- iter (n-1) f x
                       f $. y

    makeSucc n e1 =
      do r1 <- go e1
         function2 $ \s z -> r1 $. s $. (iter n s z)
    makeAdd e1 e2 =
      do r1 <- go e1
         r2 <- go e2
         function2 $ \s z -> r1 $. s $. (r2 $. s $. z)
    makeIterAdd n e1 =
      do r1 <- go e1
         function2 $ \s z ->
           do plusE1 <- r1 $. s
              iter n plusE1 z
    makeMul e1 e2 =
      do r1 <- go e1
         r2 <- go e2
         function1 $ \s -> r1 $. (r2 $. s)

-- | Church Numeral operations
gIsZero, gNatSucc, gNatAdd, gNatMult, gNatPred :: Definition
gIsZero =
  def "isZero" $
    do true' <- load gTrue
       constFalse' <- load gKKI
       function1 $ \n -> n $. constFalse' $. true'
gNatSucc =
  def "natSucc" (function3 $ \n f x -> f $. (n $. f $. x))
gNatAdd =
  def "natAdd" (function4 $ \m n f x -> m $. f $. (n $. f $. x))
-- multiplication (*)
gNatMult =
  def "natMult" (function3 $ \m n f -> m $. (n $. f))
-- predecessor (-1)
-- from Wikipedia page
--   http://en.wikipedia.org/wiki/Lambda_calculus#Arithmetic_in_lambda_calculus
gNatPred =
  def "natPred" $
    do k'  <- require gConst
       id' <- require gId
       t'  <- function3 $ \f g h -> h $. (g $. f)
       function3 $
         \n f x -> n $. (t' $. f) $. (k' $. x) $. id'

-- | Char -> church numeral
natOfChar :: Char -> Int
natOfChar c = fromIntegral (fromIntegral (ord c - ord 'w') :: Word8)

gNatOfChar :: Char -> Definition
gNatOfChar c = gNat (natOfChar c)

-------- Utilities

getDefinedCNats :: Grassify [(Int, Ref)]
getDefinedCNats =
  do dict <- gets dictionary
     return $ cnats dict
  where
    cnats dict = [(n,ref) | (CNatDec n, ref) <- Map.toAscList dict]
