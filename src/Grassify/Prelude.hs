module Grassify.Prelude(
  def, natDef,
  gId, gConst, gKI, gKKI, gFix,
  gTrue, gFalse, gNot, gAnd, gOr
) where

import           Grassify

def :: String -> Grassify Ref -> Definition
def name rhs = Def (VarDec name) rhs

natDef :: Int -> Grassify Ref -> Definition
natDef n rhs = Def (CNatDec n) rhs

-- | Functional Combinators
-- id = \x -> x (identity function)
-- const = true
-- kki = \x y z -> z
-- fix = (fixpoint operator)
gId, gConst, gKI, gKKI, gFix :: Definition
gId = def "id" (function1 $ \x -> return x)
gConst =
  def "const" $
    do id' <- require gId
       function2 $ \x _ -> id' $. x
gKI = def "ki" (function2 $ \_ y -> return y)
gKKI = def "kki" (function3 $ \_ _ z -> return z)
gFix =
  def "fix" $
    do ggx <- function2 $ \g x -> g $. g $. x
       fgg <- function2 $ \f g -> f $. (ggx $. g)
       function1 $ \f ->
         do tmp <- fgg $. f
            tmp $. tmp

-- | Church Bools
-- true = \x y -> x
-- false = \x y -> y
-- not = \x t f -> x f t
-- and = \x y -> x y false
-- or = \x y -> x true y
gTrue, gFalse, gNot, gAnd, gOr :: Definition
gTrue  = gConst
gFalse = gKI
gNot   = def "not" (function3 $ \x t f -> x $. f $. t)
gAnd =
  let and' = do false' <- load gFalse
                function2 $ \x y -> x $. y $. false'
  in def "and" and'
gOr =
  let or' = do true' <- load gTrue
               function2 $ \x y -> x $. true' $. y
  in def "or" or'
