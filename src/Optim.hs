{-# LANGUAGE BangPatterns               #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
module Optim(
  Optimizable(..),
  Optim(),
  runOptimization,
  askState,
  advance,
  branch
) where

import           Control.Arrow       (first)
import           Control.Monad

import           Data.Function       (on)
import           Data.List           (sortBy)

import           Control.Monad.State

import           ListT

-- | Optimization target.
class (Eq r) => Optimizable r where
  -- | a is better than or equally better to b.
  superior :: r -> r -> Bool

  -- | a `compareHeuristic` b == LT
  --   means a is better than b.
  compareHeuristic :: r -> r -> Ordering

-- | Optimization monad.
newtype Optim r a = Optim { runOptim :: r -> ListT (State (Maybe r)) (a, r) }

instance Functor (Optim r) where
  fmap f ma = Optim $ \s -> fmap (first f) (runOptim ma s)

instance Applicative (Optim r) where
  pure = return
  (<*>) = ap

instance Monad (Optim r) where
  return a = Optim $ \s -> return (a, s)
  ma >>= k = Optim $ \s ->
    do (a, s') <- runOptim ma s
       runOptim (k a) $! s'

runOptimization :: (Optimizable r) => Optim r a -> r -> Maybe (a, r)
runOptimization (Optim ma) start =
    evalState (loop (ma start) Nothing) Nothing
  where
    loop mar result =
      do v <- runListT mar
         case v of
           Nil -> return result
           Cons (a, r) rest ->
             do best <- get
                if superior' r best
                  then do modify' (const (Just r))
                          loop rest (Just (a,r))
                  else loop rest result

    superior' _ Nothing  = True
    superior' r (Just s) = superior r s

askState :: Optim r r
askState = Optim $ \s -> return (s, s)

advance :: Optimizable r => (r -> r) -> Optim r ()
advance f = Optim $ \s ->
  do best <- get
     let !s' = f s
     case best of
       Just r | r `superior` s' -> mzero
       _      -> return ((), s')

branch :: Optimizable r => (r -> a -> r) -> [a] -> Optim r a
branch f as = Optim $ \s ->
  do best <- get
     let nexts = fmap (\a -> (a, f s a)) as
         nexts' = sortByHeuristic nexts
         nexts'' = competeToBest best nexts'
         nexts''' = filterMinima nexts''
     choose nexts'''
  where
    sortByHeuristic = sortBy (compareHeuristic `on` snd)
    competeBy r = filter (\(_, r') -> not (r `superior` r'))
    competeToBest = maybe id competeBy
    filterMinima []             = []
    filterMinima ((a,r) : rest) = (a,r) : filterMinima (competeBy r rest)
