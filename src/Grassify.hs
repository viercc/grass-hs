{-# LANGUAGE BangPatterns               #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Grassify(
  Grassify(),
  GrassifyState(..),
  Ref(..),
  Signature(..), Definition(..),
  plantGrass, grassify, getGrassCode,
  gApp, gAbs, top,
  Applyable(..), ($.),
  function1, function2, function3, function4,
  register, load, require
) where

--import Debug.Trace
import           Control.Monad.State

import qualified Data.DList          as DL
import qualified Data.Map.Strict     as Map

import           Grass.Base

newtype Grassify a = Grassify { runGrassify :: State GrassifyState a }
   deriving (Functor, Applicative, Monad,
             MonadState GrassifyState)

-- | Reference to a value.
data Ref = GIn | GW | GSucc | GOut
         | Arg !Int | Direct !Int
         deriving (Show, Eq, Ord)

data GrassifyState =
  GrassifyState {
    depth      :: [Int],
    code       :: DL.DList GInst,
    dictionary :: Map.Map Signature Ref
  }
  deriving (Show)

data Signature = VarDec String | CNatDec Int
               deriving (Show, Eq, Ord)

data Definition = Def Signature (Grassify Ref)

---- backend functions
derefer :: Ref -> GrassifyState -> Int
derefer ref st = case ref of
  GIn      -> d - 1
  GW       -> d - 2
  GSucc    -> d - 3
  GOut     -> d - 4
  Arg n    -> d - (d' + n)
  Direct n -> d - n
  where ~(d : ~(d' : _)) = depth st

---- monad primitives
plantGrass :: Grassify a -> IO ()
plantGrass = putStrLn . grassify

grassify :: Grassify a -> String
grassify = unparseGrass . getGrassCode

getGrassCode :: Grassify a -> GrassCode
getGrassCode g =
  let initialState = GrassifyState{
          depth = [4,0],
          code = DL.empty,
          dictionary = Map.empty
        }
  in DL.toList $ code $ execState (runGrassify g) initialState

gApp :: Ref -> Ref -> Grassify Ref
gApp ref1 ref2 =
  do st <- get
     let (d:ds) = depth st
         !d' = d + 1
         newInst = App (derefer ref1 st) (derefer ref2 st)
     put st{ depth = d':ds,
             code = code st `DL.snoc` newInst }
     return (Direct d')

gAbs :: Int -> Grassify a -> Grassify Ref
gAbs numVar body =
  do st <- get
     let (d:ds) = depth st
         !d' = d + 1
         innerSt = st{ depth = (d + numVar) : d : ds,
                       code = DL.empty }
     put innerSt
     _ <- body
     generatedCode <- gets code
     let newInst = Abs numVar (DL.toList generatedCode)
     put st{ depth = d' : ds,
             code = code st `DL.snoc` newInst }
     return (Direct d')

top :: Grassify Ref
top = do (d:_) <- gets depth
         return (Direct d)

---- auxility functions
class Applyable a where
  toExpr :: a -> Grassify Ref
-- Grassify R = State GrassifyState Ref
instance Applyable (Grassify Ref) where
  toExpr = id
instance Applyable Ref where
  toExpr = return

infixl 9 $.

($.) :: (Applyable a, Applyable b) => a -> b -> Grassify Ref
a1 $. a2 = do r1 <- toExpr a1
              r2 <- toExpr a2
              gApp r1 r2

-- funcion* series: to write abs easily

function1 :: (Ref -> Grassify Ref) -> Grassify Ref
function1 f = gAbs 1 $ f (Arg 1)

function2 :: (Ref -> Ref -> Grassify Ref) -> Grassify Ref
function2 f = gAbs 2 $ f (Arg 1) (Arg 2)

function3 :: (Ref -> Ref -> Ref -> Grassify Ref) -> Grassify Ref
function3 f = gAbs 3 $ f (Arg 1) (Arg 2) (Arg 3)

function4 :: (Ref -> Ref -> Ref -> Ref -> Grassify Ref) -> Grassify Ref
function4 f = gAbs 4 $ f (Arg 1) (Arg 2) (Arg 3) (Arg 4)

---- library functionality
register :: Signature -> Ref -> Grassify ()
register sig ref =
  do dict <- gets dictionary
     let !dict' = Map.insert sig ref dict
     modify $ \st -> st{ dictionary = dict' }

load :: Definition -> Grassify Ref
load (Def sig rhs) =
  do ref <- rhs
     register sig ref
     return ref

require :: Definition -> Grassify Ref
require def@(Def sig _) =
  do dict <- gets dictionary
     case Map.lookup sig dict of
       Nothing  -> load def
       Just ref -> return ref
