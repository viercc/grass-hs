module Tree(
  Tree(..),
  -- from Foldable
  toList
  ) where

import Control.Monad (ap)
import Data.Foldable

data Tree a = Leaf a
            | Bin (Tree a) (Tree a)
            deriving (Show, Ord, Eq)

instance Functor Tree where
  fmap f (Leaf a)  = Leaf (f a)
  fmap f (Bin l r) = Bin (fmap f l) (fmap f r)

instance Applicative Tree where
  pure = return
  (<*>) = ap

instance Monad Tree where
  return = Leaf
  Leaf a  >>= k = k a
  Bin a b >>= k = Bin (a >>= k) (b >>= k)

instance Foldable Tree where
  foldr f z (Leaf a)  = f a z
  foldr f z (Bin l r) = foldr f (foldr f z l) r

