module Grass.Eval(
  module Grass.Object,
  GrassEnv,
  runEval, initialEnv,

  eval, evalHandleIO, evalPure,
  binaryIO
  ) where

import           Grass.Base
import           Grass.Object

import           Control.Exception
import           Data.Char         (chr, ord)
import           Data.Word
import           System.IO

type GrassEnv m = [GObj m]

eval :: GrassCode -> IO (GObj IO)
eval code =
  binaryIO stdin . binaryIO stdout $
    do r <- runEval initialEnv code
       r `apply` r

binaryIO :: Handle -> IO a -> IO a
binaryIO h action =
  do enc <- hGetEncoding h
     hSetBinaryMode h True
     a <- action
     case enc of
       Nothing -> return ()
       Just encmode -> hSetBinaryMode h False >>
                       hSetEncoding h encmode
     return a

evalHandleIO :: IO Handle -> IO Handle -> GrassCode -> IO ()
evalHandleIO openIn openOut code =
  bracket openIn hClose $ \hin ->
    binaryIO hin $
      bracket openOut hClose $ \hout ->
        binaryIO hout $
          evalHandleIO' hin hout
  where
    evalHandleIO' hin hout =
      do is <- hGetContents hin
         let isBytes = fmap (fromIntegral . ord) is
             (_, osBytes) = evalPure code isBytes
             os = fmap (chr . fromIntegral) osBytes
         hPutStr hout os

evalPure :: GrassCode -> [Word8] -> (GObj GPure, [Word8])
evalPure code is =
  runGPure (do { r <- runEval initialEnv code; r `apply` r }) is

runEval :: MonadGrass m => GrassEnv m -> GrassCode -> m (GObj m)
runEval = runEval'
  where
    runEval' env [] = return (head env)
    runEval' env (c:cs) =
      do r <- step env c
         runEval (r:env) cs

    step env (App m n) = (env !! m) `apply` (env !! n)
    step env (Abs n c) = makeAbs env n c

    makeAbs :: MonadGrass m => GrassEnv m -> Int -> GrassCode -> m (GObj m)
    makeAbs env n c =
      if (n <= 0) then
        runEval env c
      else
        return $ GFunc (\x -> makeAbs (x:env) (n-1) c)

-- Primitives
initialEnv :: MonadGrass m => GrassEnv m
initialEnv = [gOut, gSucc, gW, gIn]

