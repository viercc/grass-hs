{-# LANGUAGE RankNTypes #-}
module Grass.Object(
  MonadGrass(..),
  GPure(), runGPure,
  GObj(..),
  apply,
  glam1, glam2, glam3, glam4,
  gOut, gSucc, gW, gIn
) where

import           Control.Monad

import           Data.Char     (chr, ord)
import           Data.Word

import           System.IO

-- | Class of Monad which can evaluate Grass
class Monad m => MonadGrass m where
  getByte :: m (Maybe Word8)
  putByte :: Word8 -> m ()

instance MonadGrass IO where
  getByte =
    do eof <- isEOF
       if eof
         then return Nothing
         else Just . fromIntegral . ord <$> getChar
  putByte b = putChar $ chr $ fromIntegral b

-- | Pure evaluator of Grass
newtype GPure a = GPure {
    runGPure_ :: [Word8] -> [Word8] -> (a, [Word8], [Word8])
  }

runGPure :: GPure a -> [Word8] -> (a, [Word8])
runGPure ma is = let (a, _, os) = runGPure_ ma is []
                 in (a, os)

instance Functor GPure where
  fmap = liftM

instance Applicative GPure where
  pure = return
  (<*>) = ap

instance Monad GPure where
  return a = GPure $ \is os -> (a, is, os)
  ma >>= k = GPure $ \is os ->
    let (a, is', os'') = runGPure_ ma is os'
        (b, is'', os') = runGPure_ (k a) is' os
    in (b, is'', os'')

instance MonadGrass GPure where
  getByte = GPure $ \is os ->
    case is of
      []      -> (Nothing, [], os)
      (i:is') -> (Just i, is', os)
  putByte b = GPure $ \is os -> ((), is, b:os)

-- Grass Object
data GObj m = GFunc !(GObj m -> m (GObj m))
            | GByte !Word8

-- apply first GObj to second GObj.
-- the application may cause IO action.
apply :: Monad m => GObj m -> GObj m -> m (GObj m)
apply x y = case x of
  GFunc f -> f $! y
  GByte b -> return (byteEq b y)

byteEq :: (Monad m) => Word8 -> GObj m  -> GObj m
byteEq _ (GFunc _) = gFalse
byteEq x (GByte y) = if x == y then gTrue else gFalse

gTrue, gFalse :: Monad m => GObj m
gTrue  = glam2 (\x _ -> return x)
gFalse = glam2 (\_ y -> return y)

glam1 :: Monad m => (GObj m -> m (GObj m)) -> GObj m
glam1 = GFunc

glam2 :: Monad m => (GObj m -> GObj m -> m (GObj m)) -> GObj m
glam2 f = GFunc $ \x -> return $ glam1 (f x)

glam3 :: Monad m => (GObj m -> GObj m -> GObj m -> m (GObj m)) -> GObj m
glam3 f = GFunc $ \x -> return $ glam2 (f x)

glam4 :: Monad m => (GObj m -> GObj m -> GObj m -> GObj m -> m (GObj m)) ->
                    GObj m
glam4 f = GFunc $ \x -> return $ glam3 (f x)

-- Primitives
gOut, gSucc, gW, gIn :: MonadGrass m => GObj m
gOut = GFunc primOut where
  primOut (GFunc _)   = fail "gOut: not a byte"
  primOut x@(GByte b) = putByte b >> return x

gSucc = GFunc primSucc where
  primSucc (GFunc _) = fail "gSucc: not a byte"
  primSucc (GByte x) = return $ GByte (1 + x)

gW = GByte 119

gIn = GFunc primIn where
  primIn feof = maybe feof GByte <$> getByte
