{-# LANGUAGE FlexibleContexts #-}
module Grass.Named(
  VarName,
  NamedGInst(..),
  NamedGrassCode,
  toNamed
) where

import           Control.Monad.State
import           Grass.Base

type VarName = String

data NamedGInst =
    NApp VarName VarName VarName
  | NAbs VarName [VarName] NamedGrassCode
  | Return VarName
  deriving (Show)

type NamedGrassCode = [NamedGInst]

primSymbols :: [VarName]
primSymbols = ["gOut", "gSucc", "gW", "gIn"]

uniqueNames :: [VarName]
uniqueNames = [ a:p | p <- suffixes, a <- alphabets ]
  where
    alphabets = "abcdefghjklmnpqrstuvwxyz"
    suffixes = [[]] ++ fmap show ([1..] :: [Integer])

toNamed :: GrassCode -> NamedGrassCode
toNamed code = evalState (convertM primSymbols code) uniqueNames
  where
    freshName = do (x:xs) <- get
                   put xs
                   return x

    convertM :: [VarName] -> GrassCode -> State [VarName] [NamedGInst]
    convertM env [] = return [Return (head env)]
    convertM env (c:cs) =
      do (x, namedC) <- convertOne env c
         namedCS <- convertM (x:env) cs
         return (namedC:namedCS)

    convertOne :: [VarName] -> GInst -> State [VarName] (VarName, NamedGInst)
    convertOne env (App n m) =
      do x <- freshName
         return (x, NApp x (env !! n) (env !! m))
    convertOne env (Abs n ds) =
      do x <- freshName
         vars <- replicateM n freshName
         namedDs <- convertM (reverse vars ++ env) ds
         return (x, NAbs x vars namedDs)
