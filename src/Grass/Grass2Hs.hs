{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module Grass.Grass2Hs(
  translate
) where

import           Text.Printf
import           Data.Text.Prettyprint.Doc
import           Template

import           Grass.Base
import           Grass.Named

import           Text.Heredoc

template :: Template
template = readTemplate [there|src/Grass/grass2hs.hstemplate|]

translate :: GrassCode -> String
translate code =
    show $ fillTemplate template (filler mainCode rollFuncs)
  where
    hsCode = toNamed code
    mainCode = translateMain hsCode
    rollFuncs = makeCurryFuncs $ maxAbstArity code

filler :: Doc a -> Doc a ->
          (String -> Maybe (Doc a))
filler mainCode rollFuncs name = case name of
  "code"       -> Just mainCode
  "roll_funcs" -> Just rollFuncs
  _            -> Nothing

maxAbstArity :: GrassCode -> Int
maxAbstArity code = maximum $ map abstArity code
  where abstArity (App _ _) = 0
        abstArity (Abs n _) = n

makeCurryFuncs :: Int -> Doc a
makeCurryFuncs n = vcat $ map curryfs [1..n]
  where
  curryfs 1 = "lam1 = GFunc"
  curryfs k = pretty (printf fmt k (k-1) :: String)
  fmt = "lam%d f = GFunc $ \\x -> return $ lam%d (f x)"

translateMain :: NamedGrassCode -> Doc a
translateMain = trProg
  where
    trProg :: [NamedGInst] -> Doc a
    trProg cs = "do" <+> align (vcat (map trElem cs))

    trElem (NApp name e1 e2) =
      if name == ""
        then pretty e1 <+> "!" <+> pretty e2
        else pretty name <+> "<-" <+> pretty e1 <+> "!" <+> pretty e2
    trElem (NAbs name args body) =
      let n = length args
          strArgs = unwords args
          bodyDoc = 
            pretty (printf "lam%d (\\%s ->" n strArgs :: String) <#>
            indent 2 (trProg body) <#>
            ")"
      in "let" <+> pretty name <+> "=" <+> align bodyDoc
    trElem (Return retVal) = "return" <+> pretty retVal

(<#>) :: Doc a -> Doc a -> Doc a
a <#> b = vsep [a,b]

infixr 9 <#>
