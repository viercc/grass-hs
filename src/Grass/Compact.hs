{-# LANGUAGE BangPatterns #-}
module Grass.Compact(
         CompactGInst(..),
         CompactGrassCode,
         compact,
         wrap, deleteUnreachable, inline
       ) where

import           Data.List       (foldl')
import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set

import           Grass.Named
import           Tree

data CompactGInst = CApp VarName (Tree VarName)
                  | CAbs VarName [VarName] CompactGrassCode
                  | CReturn VarName
                  deriving (Show)
type CompactGrassCode = [CompactGInst]

compact :: NamedGrassCode -> CompactGrassCode
compact = inline . deleteUnreachable . wrap

wrap :: NamedGrassCode -> CompactGrassCode
wrap = fmap wrapOne
  where wrapOne (NApp x y z)   = CApp x (Bin (Leaf y) (Leaf z))
        wrapOne (NAbs x vs ds) = CAbs x vs (wrap ds)
        wrapOne (Return x)     = CReturn x

deleteUnreachable :: CompactGrassCode -> CompactGrassCode
deleteUnreachable code =
  let (depends, start) = makeDepends code
      reachableVars = reachableSet depends start
      needed (CApp x _)   = x `Set.member` reachableVars
      needed (CAbs x _ _) = x `Set.member` reachableVars
      needed (CReturn _)  = True

      insideAbs (CAbs x vars ds) = CAbs x vars (deleteUnreachable ds)
      insideAbs c                = c
  in filter needed . fmap insideAbs $ code

makeDepends :: CompactGrassCode -> (Map.Map VarName [VarName], VarName)
makeDepends = foldl' step (Map.empty, error "No CReturn")
  where
    step (!dep, start) (CApp x expr)=
      (Map.insert x (toList expr) dep, start)
    step (!dep, _) (CReturn x) = (dep, x)
    step r _ = r

reachableSet :: (Ord a) => Map.Map a [a] -> a -> Set.Set a
reachableSet dep start = loop Set.empty [start]
  where
    loop visited [] = visited
    loop visited (a:as) =
      case a `Set.member` visited of
        True -> loop visited as
        False ->
          let visited' = Set.insert a visited
              next = Map.findWithDefault [] a dep
          in loop visited' (next ++ as)

inline :: CompactGrassCode -> CompactGrassCode
inline code =
  let defs = appDefs code
      usage = varUsage code
      inlineableSet = Map.keysSet $ Map.filter (<= 1) usage
      subst = foldr composeSubst emptySubst $
                fmap toSubst $ Set.toList inlineableSet

      toSubst x = case Map.lookup x defs of
                    Nothing   -> emptySubst
                    Just expr -> Map.singleton x expr

      inlineOne (CApp x expr) =
        case x `Set.member` inlineableSet of
          True  -> []
          False -> [CApp x (applySubst subst expr)]
      inlineOne (CAbs x vars ds) = [CAbs x vars (ds >>= inlineOne)]
      inlineOne c = [c]
  in code >>= inlineOne

appDefs :: CompactGrassCode -> Map.Map VarName (Tree VarName)
appDefs = foldl' step Map.empty
  where
    step defs (CApp x expr) = Map.insert x expr defs
    step defs (CAbs _ _ ds) = Map.union defs (appDefs ds)
    step defs _             = defs

varUsage :: CompactGrassCode -> Map.Map VarName Int
varUsage = foldl' step zero
  where
    zero = Map.empty
    merge = Map.unionWith (+)
    countUp defs a = merge defs (Map.singleton a 1)
    step defs (CApp _ expr) = foldl' countUp defs $ toList expr
    step defs (CAbs _ _ ds) = merge defs (varUsage ds)
    step defs (CReturn x)   = countUp defs x

type Subst a = Map.Map a (Tree a)

emptySubst :: Subst a
emptySubst = Map.empty

applySubst :: (Ord a) => Subst a -> Tree a -> Tree a
applySubst subst expr =
  expr >>= \a -> Map.findWithDefault (Leaf a) a subst

composeSubst :: (Ord a) => Subst a -> Subst a -> Subst a
composeSubst s t = Map.union s (Map.map (applySubst s) t)
