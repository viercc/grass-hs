module Grass.Base(
  GInst(..), GrassCode,
  parseGrass, unparseGrass
) where

import           Data.List                    (find, unfoldr)
import           Text.ParserCombinators.ReadP

-- Grass Code Instructions (Application, Abstraction)
-- Note: WW...(m times)...WW ww...(n times)...ww
--       is translated to (App (m-1) (n-1))
--       for 0-starting indexing of list
data GInst = App Int Int
           | Abs Int GrassCode
           deriving (Show, Eq, Ord)
type GrassCode = [GInst]

parseGrass :: String -> GrassCode
parseGrass str = maybe [] fst $
                 find (\(_,r) -> r == "") $
                 readP_to_S grassParser str

unparseGrass :: GrassCode -> String
unparseGrass code = unparse code ""
  where
    unparse = intercalateS v . fmap unparseInsts . splitCode

    intercalateS sep = foldr (\a r -> a . sep . r) id

    splitCode = unfoldr step
      where
        step []              = Nothing
        step (Abs n d : cs') = Just ([Abs n d], cs')
        step cs              = Just (span isApp cs)

        isApp (App _ _) = True
        isApp _         = False

    unparseInst (App m n) = rep_W (m+1) . rep_w (n+1)
    unparseInst (Abs n c) = rep_w n . unparseInsts c
    unparseInsts = foldr (.) id . fmap unparseInst

    rep_w n = ((replicate n 'w')++)
    rep_W n = ((replicate n 'W')++)
    v       = ('v':)

grassParser :: ReadP GrassCode
grassParser = do x <- sepBy (progAbs +++ progApp) charv
                 optional charv
                 skipComment
                 return $ concat x
  where
  is_W c = (c == 'W' || c == 'Ｗ')
  is_w c = (c == 'w' || c == 'ｗ')
  is_v c = (c == 'v' || c == 'ｖ')

  skipComment = const () <$> munch (\c -> not (is_W c || is_w c || is_v c))
  charWL = skipComment >> (satisfy is_W)
  charws = skipComment >> (satisfy is_w)
  charv  = skipComment >> (satisfy is_v)

  runof p = length <$> many1 p
  oneApp = do numWL <- runof charWL
              numws <- runof charws
              return (App (numWL-1) (numws-1))
  progApp = many oneApp
  progAbs = do numVar <- runof charws
               body <- progApp
               return [Abs numVar body]
