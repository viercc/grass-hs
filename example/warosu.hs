module Main(main) where

import System.IO
import Data.Char (chr, ord)
import Data.Word

data GObj = GFunc !(GObj -> IO GObj)
          | GByte !Word8

main :: IO ()
main =
  do hSetBinaryMode stdin True
     hSetBinaryMode stdout True
     x <- mainCode
     x ! x
     return ()

mainCode :: IO GObj
mainCode =
    do let a = lam4 (\b c d e ->
                 do return e
               )
       let f = lam1 (\g ->
                 do return g
               )
       let h = lam2 (\j k ->
                 do l <- j ! k
                    return l
               )
       let m = lam2 (\n p ->
                 do q <- p ! n
                    return q
               )
       let r = lam4 (\s t u v ->
                 do w <- t ! u
                    x <- w ! v
                    y <- s ! u
                    z <- y ! x
                    return z
               )
       let a1 = lam1 (\b1 ->
                  do c1 <- r ! b1
                     d1 <- c1 ! b1
                     return d1
                )
       let e1 = lam1 (\f1 ->
                  do g1 <- r ! h
                     h1 <- g1 ! h
                     j1 <- g1 ! h1
                     k1 <- g1 ! j1
                     l1 <- a1 ! k1
                     m1 <- r ! j1
                     n1 <- m1 ! l1
                     p1 <- r ! n1
                     q1 <- p1 ! k1
                     r1 <- a1 ! l1
                     s1 <- a1 ! n1
                     t1 <- a1 ! q1
                     u1 <- a1 ! r1
                     v1 <- a1 ! u1
                     w1 <- m ! t1
                     x1 <- w1 ! h1
                     y1 <- r ! q1
                     z1 <- y1 ! x1
                     a2 <- f ! f
                     b2 <- a2 ! gW
                     c2 <- a2 ! gSucc
                     d2 <- n1 ! c2
                     e2 <- d2 ! b2
                     f2 <- u1 ! c2
                     g2 <- f2 ! e2
                     h2 <- v1 ! c2
                     j2 <- h2 ! b2
                     k2 <- s1 ! c2
                     l2 <- k2 ! j2
                     m2 <- t1 ! c2
                     n2 <- m2 ! l2
                     p2 <- h1 ! c2
                     q2 <- p2 ! n2
                     r2 <- z1 ! c2
                     s2 <- r2 ! b2
                     t2 <- a2 ! gOut
                     u2 <- t2 ! e2
                     v2 <- t2 ! l2
                     w2 <- t2 ! u2
                     x2 <- t2 ! g2
                     y2 <- t2 ! w2
                     z2 <- t2 ! v2
                     a3 <- t2 ! y2
                     b3 <- t2 ! x2
                     c3 <- t2 ! a3
                     d3 <- t2 ! q2
                     e3 <- a2 ! t2
                     f3 <- e3 ! c3
                     g3 <- e3 ! n2
                     h3 <- e3 ! f3
                     j3 <- e3 ! j2
                     k3 <- e3 ! h3
                     l3 <- e3 ! d3
                     m3 <- e3 ! k3
                     n3 <- e3 ! g3
                     p3 <- e3 ! m3
                     q3 <- e3 ! j3
                     r3 <- e3 ! s2
                     return r3
                )
       return e1
  where
    lam1 = GFunc
    lam2 f = GFunc $ \x -> return $ lam1 (f x)
    lam3 f = GFunc $ \x -> return $ lam2 (f x)
    lam4 f = GFunc $ \x -> return $ lam3 (f x)

(!) :: GObj -> GObj -> IO GObj
GFunc x ! y       = x $! y
GByte x ! GByte y | x == y = return $ gTrue
GByte _ ! _                = return $ gFalse

gTrue, gFalse :: GObj
gTrue  = GFunc $ \x -> return $ GFunc $ \_ -> return x
gFalse = GFunc $ \_ -> return $ GFunc $ \y -> return y

gOut, gSucc, gW, gIn :: GObj
gOut = GFunc primOut where
  primOut (GFunc _)   = fail "gOut: not a byte"
  primOut x@(GByte b) = putByte b >> return x
gSucc = GFunc primSucc where
  primSucc (GFunc _) = fail "gSucc: not a byte"
  primSucc (GByte x) = return $ GByte (1 + x)
gW = GByte 119
gIn = GFunc primIn where
  primIn feof = maybe feof GByte <$> getByte

putByte :: Word8 -> IO ()
putByte = putChar . chr . fromIntegral

getByte :: IO (Maybe Word8)
getByte =
  do eof <- isEOF
     if eof
       then return Nothing
       else Just . fromIntegral . ord <$> getChar