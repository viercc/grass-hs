module Main(main) where

import System.IO
import Data.Char (chr, ord)
import Data.Word

data GObj = GFunc !(GObj -> IO GObj)
          | GByte !Word8

main :: IO ()
main =
  do hSetBinaryMode stdin True
     hSetBinaryMode stdout True
     x <- mainCode
     x ! x
     return ()

mainCode :: IO GObj
mainCode =
    do let a = lam3 (\b c d ->
                 do return d
               )
       let e = lam1 (\f ->
                 do g <- gIn ! a
                    h <- g ! g
                    j <- h ! gOut
                    k <- j ! a
                    l <- k ! g
                    m <- h ! f
                    n <- m ! k
                    p <- n ! f
                    return p
               )
       return e
  where
    lam1 = GFunc
    lam2 f = GFunc $ \x -> return $ lam1 (f x)
    lam3 f = GFunc $ \x -> return $ lam2 (f x)

(!) :: GObj -> GObj -> IO GObj
GFunc x ! y       = x $! y
GByte x ! GByte y | x == y = return $ gTrue
GByte _ ! _                = return $ gFalse

gTrue, gFalse :: GObj
gTrue  = GFunc $ \x -> return $ GFunc $ \_ -> return x
gFalse = GFunc $ \_ -> return $ GFunc $ \y -> return y

gOut, gSucc, gW, gIn :: GObj
gOut = GFunc primOut where
  primOut (GFunc _)   = fail "gOut: not a byte"
  primOut x@(GByte b) = putByte b >> return x
gSucc = GFunc primSucc where
  primSucc (GFunc _) = fail "gSucc: not a byte"
  primSucc (GByte x) = return $ GByte (1 + x)
gW = GByte 119
gIn = GFunc primIn where
  primIn feof = maybe feof GByte <$> getByte

putByte :: Word8 -> IO ()
putByte = putChar . chr . fromIntegral

getByte :: IO (Maybe Word8)
getByte =
  do eof <- isEOF
     if eof
       then return Nothing
       else Just . fromIntegral . ord <$> getChar