module Main(main) where

import System.IO
import Data.Char (chr, ord)
import Data.Word

data GObj = GFunc !(GObj -> IO GObj)
          | GByte !Word8

main :: IO ()
main =
  do hSetBinaryMode stdin True
     hSetBinaryMode stdout True
     x <- mainCode
     x ! x
     return ()

mainCode :: IO GObj
mainCode =
    do let a = lam1 (\b ->
                 do c <- gOut ! gW
                    d <- b ! b
                    return d
               )
       return a
  where
    lam1 = GFunc

(!) :: GObj -> GObj -> IO GObj
GFunc x ! y       = x $! y
GByte x ! GByte y | x == y = return $ gTrue
GByte _ ! _                = return $ gFalse

gTrue, gFalse :: GObj
gTrue  = GFunc $ \x -> return $ GFunc $ \_ -> return x
gFalse = GFunc $ \_ -> return $ GFunc $ \y -> return y

gOut, gSucc, gW, gIn :: GObj
gOut = GFunc primOut where
  primOut (GFunc _)   = fail "gOut: not a byte"
  primOut x@(GByte b) = putByte b >> return x
gSucc = GFunc primSucc where
  primSucc (GFunc _) = fail "gSucc: not a byte"
  primSucc (GByte x) = return $ GByte (1 + x)
gW = GByte 119
gIn = GFunc primIn where
  primIn feof = maybe feof GByte <$> getByte

putByte :: Word8 -> IO ()
putByte = putChar . chr . fromIntegral

getByte :: IO (Maybe Word8)
getByte =
  do eof <- isEOF
     if eof
       then return Nothing
       else Just . fromIntegral . ord <$> getChar